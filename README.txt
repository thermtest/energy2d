This is a fork of the original code located at: https://github.com/charxie/energy2d

A simple API to search a thermal properties database was added. In order to compile from source you will need to implement a web API. The API will need to provide a JSON response with these fields:
Name
Conductivity
Specific_Heat
Density

You will then need to add your API url to TTDatabaseAPI.java on line 44 stored in the String url

set the following JVM parameter to disable the GetDown update check so as to not overright the database module:

-DNoUpdate=true