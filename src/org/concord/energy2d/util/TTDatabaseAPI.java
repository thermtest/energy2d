package org.concord.energy2d.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

import org.json.*;

import javax.swing.JOptionPane;


public class TTDatabaseAPI {

  private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONArray json = new JSONArray (jsonText);
      return json;
    } finally {
      is.close();
    }
  }
  
  public static Object[][] Search(String term) throws JSONException
  {
      JSONArray json;
      try{
          String url = "Your API URL";
      json = readJsonFromUrl(url + term);

      }
      catch(Exception e)
      {
         JOptionPane.showMessageDialog(null,"Error connecting to databas, Please check you internet connection and try again: " + e);
          Object rowData[][] = { { null, null, null } };
          return rowData;
      }
   
     
     Object[][] returnstring = new Object[json.length()][4];
    for (int i=0; i<json.length(); i++) {
        JSONObject item = json.getJSONObject(i);
        
        returnstring[i][0] = item.get("Name");
        returnstring[i][1] = item.get("Conductivity");
        returnstring[i][2] = item.get("Specific_Heat");
        returnstring[i][3] = item.get("Density");
}
    
    return returnstring;
   }  
        
}